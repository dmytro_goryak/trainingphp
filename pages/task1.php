<script src="scrypt/input-spinner.js"></script>


<div class="task px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Task 1</h1>
    <form id="form">
        <p class="lead">Please input number between 1-10:</p>
        <p><input type="number" id="inputCount" value="0" min="0" max="10" step="1" /></p>
        <div class="newInput">
            <p class="lead" id="list"></p>
        </div>
        <input type="submit" id="btnSubmit" class="btn btn-primary mb-2" disabled="disabled" value="Download result">
    </form>
</div>

<script src="scrypt/main.js"></script>