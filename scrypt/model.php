<?php
class Permutatation
{
    private $collectionValues = array(array());

    function __construct($inputsArray)
    {
        //cycle by each input
        foreach ($inputsArray as $ikey => $ivalue) {
            //delete all spaces and create new array where each element are splited by comma
            $tempArray = explode(',', str_replace(' ', '', $ivalue));

            //if it letters-input than use arraydiff between alphabetic and user's letters
            if ($ikey < 2) $tempArray =  array_diff(range('a', 'z'), $tempArray);
            //if it numbers-input than delete duplicate decimal
            else $tempArray =  array_unique($tempArray);

            //cycle by each element in input (collect all element for permutation)
            foreach ($tempArray as $jkey => $jvalue) {
                $this->collectionValues[$ikey][$jkey] = $jvalue;
            }
        }
    }
    function getResult()
    {
        $count = count($this->collectionValues);
        $tempArray = array();
        // if array have more than 1 object->array to permutate with others
        if ($count > 1) {
            //last inputs in object 'collectionValues' BUT FIRST in user interface
            $tempArray = $this->collectionValues[$count - 1];
            //index of SECOND inputs in user interface
            $x = $count - 2;
            do {
                //mixing 2 inputs in one
                $tempArray = $this->mixingArrays($tempArray, $this->collectionValues[$x]);
                $x--;
            } while ($x >= 0);
        } else {
            //this array have just 1 object->array (so its just one input with letters)
            $tempArray = $this->collectionValues[0];
        }

        //convert result to ONE string 
        $resultString = '';
        foreach ($tempArray as $value) {
            $resultString .= $value . "\n";
        }

        return $resultString;
    }

    private function mixingArrays($leftArray, $rightArray)
    {
        $i = 0;
        foreach ($leftArray as $left) {
            foreach ($rightArray as $right) {
                $tempArray[$i] = $left . $right;
                $i++;
            }
        }
        return $tempArray;
    }
}
