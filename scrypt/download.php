<?php
//results folder
$directory = "results/";

//check if the get variable exists
if (isset($_GET['file'])) {
    try {
        print json_encode(downloadFile($directory . $_GET['file']));
    } catch (Exception $e) {
        header('HTTP/1.1 500 Internal Server Booboo');
        header('Content-Type: application/json; charset=UTF-8');
        die(json_encode(array('message' =>  $e->getMessage() . ". Issue with downloading file ", 'code' => 500)));
    }
}

//filename in uniqe for every client query
function downloadFile($fileDir)
{
    header('Content-Description: File Transfer');
    header('Content-Type: text/plain');
    //this file name is returned to the client so it's hardcode value
    header('Content-Disposition: attachment; filename="result.txt"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($fileDir));
    //file result to client
    readfile($fileDir);
    //delete file with result
    unlink($fileDir);
    exit;
}
