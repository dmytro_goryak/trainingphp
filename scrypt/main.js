//wait for full loading page
$(document).ready(function () {

    //upgrade visual for input[number]
    $("input[type='number']").inputSpinner();
    // variable to hold request
    var request;

    //submit the form and use Ajax for POST method
    $("#form").on("submit", function (e) {
        e.preventDefault();

        // abort any pending request
        if (request) request.abort();

        $form = $(this).find("input");
        $inputs = $(this).find("input.custom-input");

        //create uniqe fileId
        $fileName = Math.random().toString(36).substr(2, 16);

        //getting array with input value
        $inputsArray = [];
        for ($element of $inputs) {
            $inputsArray.unshift($element.value);
        }

        // Disabled form elements
        $form.prop("disabled", true);

        // fire off the request to function.php
        request = $.ajax({
            url: "scrypt/function.php",
            type: "post",
            data: {
                'inputsData': $inputsArray,
                'fileName': $fileName
            },
            dataType: "json",
            success: function (response) {
                //response not needed in our script, because the result is already in the file, so whatever ¯\_(ツ)_/¯
                window.location.href = "scrypt/download.php?file=" + $fileName;
            },
            error: function (textStatus, errorThrown) {
                console.error("The following error occurred: ", textStatus, errorThrown);
            },
            complete: function () {
                // Reenable the inputs
                $form.prop("disabled", false);
            }
        });
    });

    //event handler for the changed value of the number of inputs
    $("#inputCount").on("input", function (event) {
        $count = $(".inputArea").length;
        $newValue = parseInt($(this).val());

        if (!isNaN($newValue)) {
            switch ($newValue) {
                case 0:
                    $("#list").text("");
                    break;
                case 1:
                    $("#list").text("List:");
                    break;
                default:
                    $("#list").text("Lists:");
            };

            if ($count > $newValue) {
                if ($newValue == 0) $("#btnSubmit").attr("disabled", true);
                removeInputs($count, $newValue);
            } else {
                $("#btnSubmit").attr("disabled", false);
                addInputs($count, $newValue);
            };
        };
    });

    //remove block with inputs
    function removeInputs(fromIndex, toIndex) {
        for (var i = fromIndex; i > toIndex; i--) {
            $("p#inputBlock" + i).remove();
        };
    };

    //add new block with inputs
    function addInputs(fromIndex, toIndex) {
        for (var i = fromIndex + 1; i <= toIndex; i++) {
            if (i <= 2)
                //last 2 input are Letters
                $("#list").after("<p class='inputArea' id='inputBlock" + i +
                    "'><input type='text' class='form-control custom-input'" +
                    " required pattern='^\\s*[a-z]{1}(\\s*,\\s*[a-z]{1})*\\s*$'" +
                    " placeholder='a, b, c ... (letters)' required aria-label='Letters'" +
                    " title='Input can contain multiple letter but must separated by comma ! Like: f,b,i'></p>"
                );
            else
                //else inputs are Numbers
                $("#list").after("<p class='inputArea' id='inputBlock" + i +
                    "'><input type='text' class='form-control custom-input'" +
                    " required pattern='^\\s*\\d{1}(\\s*,\\s*\\d{1})*\\s*$' placeholder='1, 2, 3 ... (numbers)'" +
                    " required aria-label='Numbers'  title='Input can contain multiple digit but must separated by comma ! Like: 2,2,8'></p>"
                );
        };
    };
});