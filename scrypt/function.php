<?php
include_once 'model.php';

//get data if they declared in POST query 
$myArray = isset($_POST['inputsData']) ? $_POST['inputsData'] : null;
$fileName = isset($_POST['fileName']) ? $_POST['fileName'] : null;



//check if array not empty
if (!empty($myArray) || !empty($fileName)) {

    $permut = new Permutatation($myArray);
    $result = $permut->getResult();

    //create file with result [in folder results]
    $myfile = fopen("results/" . $fileName, "w") or die("Unable to open file!");
    fwrite($myfile, $result);
    fclose($myfile);

    //return Ajax response
    print json_encode($result);
}
//if array empty than throw error exception
else {
    header('HTTP/1.1 500 Internal Server Booboo');
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array('message' => 'Array or filename are empty ! Server cant properly calculate client query. ', 'code' => 500)));
}
